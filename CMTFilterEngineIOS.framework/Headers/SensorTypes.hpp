#pragma once

#ifndef ANDROID_SENSOR_H
enum {
    ASENSOR_TYPE_ACCELEROMETER      = 1,
    ASENSOR_TYPE_MAGNETIC_FIELD     = 2,
    ASENSOR_TYPE_GYROSCOPE          = 4,
    ASENSOR_TYPE_LIGHT              = 5,
    ASENSOR_TYPE_PRESSURE           = 6,
    ASENSOR_TYPE_PROXIMITY          = 8,
    ASENSOR_TYPE_GRAVITY            = 9,
    ASENSOR_TYPE_ROTATION           = 11,
    ASENSOR_TYPE_TAG_ACCEL          = 101,
    ASENSOR_TYPE_TAG_ACCELTS        = 102,
    ASENSOR_TYPE_OBD_SPEED          = 103,
    ASENSOR_TYPE_OBD_RPM            = 104,
    ASENSOR_TYPE_IMU_ORIENTATION    = 105
};
#endif

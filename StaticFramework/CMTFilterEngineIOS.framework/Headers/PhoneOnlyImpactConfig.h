#pragma once
////configuration for phone-only impact detection algorithm
typedef struct CMTFilterEnginePhoneOnlyImpactConfig_t {
    bool impactDetectionEnabled;
    double impactJerkThreshold;
    int lowSpeedThreshold;
    int changeSpeedThreshold;
    int changeSpeedSecondsBefore;
    int changeSpeedSecondsAfter;
    int lowSpeedWindow;
    int slowDownWindow;
    int impactSleepTicks;
    double ewmaGravAlphaValue;
    double ewmaGravResetThreshold;  
    double ewmaAccelAlphaValue;
    double ewmaAccelResetThreshold;  

#ifdef __cplusplus
    CMTFilterEnginePhoneOnlyImpactConfig_t& operator =(const CMTFilterEnginePhoneOnlyImpactConfig_t& a) {
        impactDetectionEnabled = a.impactDetectionEnabled;
        impactJerkThreshold = a.impactJerkThreshold;
        lowSpeedThreshold = a.lowSpeedThreshold;
        changeSpeedThreshold = a.changeSpeedThreshold;
        changeSpeedSecondsBefore = a.changeSpeedSecondsBefore;
        changeSpeedSecondsAfter = a.changeSpeedSecondsAfter;
        lowSpeedWindow = a.lowSpeedWindow;
        slowDownWindow = a.slowDownWindow;
        impactSleepTicks = a.impactSleepTicks;
        ewmaGravAlphaValue = a.ewmaGravAlphaValue;
        ewmaGravResetThreshold = a.ewmaGravResetThreshold;
        ewmaAccelAlphaValue = a.ewmaAccelAlphaValue;
        ewmaAccelResetThreshold = a.ewmaAccelResetThreshold;
        return *this;
    }
#endif

} CMTFilterEnginePhoneOnlyImpactConfig_t;

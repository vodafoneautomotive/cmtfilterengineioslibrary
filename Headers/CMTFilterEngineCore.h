//
//  CMTFilterEngineCore.h
//  CMTFilterEngineIOS
//
//  Created by Eugene on 3/12/14.
//  Copyright (c) 2014-2017 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "EngineEventType.h"
#include "PhoneOnlyImpactConfig.h"

typedef struct CMTFilterEngineConfig_t {
    int hasGyro;
    int hasMag;
    int hasPress;
    int hasGrav;
    int dumpRaw;
    NSString *gyroDataFormatString;
} CMTFilterEngineConfig_t;

// Note: values must match TicksRingBuffers in TickWriter.hpp
typedef NS_ENUM(NSInteger, CMTTickRingBufferType) {
  CMTTickRingBufferTypeImpact = 0,
  CMTTickRingBufferTypePanic = 1,
};

// Used to tell Filter Engine where to redirect data when pushing to ticks.
//   These can be OR'd together. A bitmask ultimately gets converted to a
//   decimal-digit character and concatenated to a key string; the result
//   is a string of the form "{key}:{redirect}". If no value is provided,
//   the default value 7 (all streams) is used.
//
// These values need to agree with the TicksOutput type defined in
//   cmt/filterengine/inc/TickWriter.hpp
//
typedef NS_ENUM(uint8_t, CMTFilterEngineRedirectCode) {
    CMTFilterEngineRedirectCodeNone     = 0,  // send to nowhere (pretty useless)
    CMTFilterEngineRedirectCodeImpact   = 1,  // send to impact stream (processed by vtrack impact calls)
    CMTFilterEngineRedirectCodeTickfile = 2,  // send to tickfile (compressed and uploaded to S3 periodically)
    CMTFilterEngineRedirectCodePanic    = 4   // send to panic stream (processed by vtrack panic calls)
};

typedef void (^EngineBlockCallback)(EngineEventType t,void *a);

typedef struct {
    float planarMag;
    uint32_t duration_ms;
    uint32_t logOffset;
    uint64_t guid;
    uint32_t impactCount;
    uint32_t secondsAgo;
    double ts;
} CMTImpactEvent;

/**
 CMTFilterEngineCore is not intended to be used directly by CMT SDK app developers. However,
 encapsulating the module inside of the CMTMobileLibrary is inconvenient. Thus, it and its methods
 are exposed. App developers should not use this interface at all.
 */
@interface CMTFilterEngineCore : NSObject

-(instancetype) init NS_UNAVAILABLE;

-(instancetype )initWithFilePrefix:(NSString *)filePrefix andEtcPrefix:(NSString *)etcPrefix
                      withDuration:(NSInteger)fileSeconds
                 withConfiguration:(CMTFilterEngineConfig_t *)ec
                    withJsonConfig:(NSString *)jTree
                 withCallbackBlock:(EngineBlockCallback)block NS_DESIGNATED_INITIALIZER;

-(NSString *) getVersion;

// get some stats about filter engine
-(void)logFilterEngineTimes;

// modify the configuration, including JSON config
-(void)setConfiguration:(CMTFilterEngineConfig_t *)filterConfig jsonConfig:(NSString *)jTree;

// set log level of filter engine
-(void)setLogLevel:(int)level;

//set the current server timestamp
-(void) pushServerTimestamp:(uint64_t)serverTimestampMillis;

// push location sample (deprecated - use pushLocationAsDictionary:isGPS)
-(void) pushLatitude:(double)lat andLongitude:(double)lon withSpeed:(float)speed withHeading:(float)heading withAltitude:(float)alt withHorizAccuracy:(float)acc withTimestampMillis:(uint64_t)timestampMillis;

// push location sample as a dictionary
-(void)pushLocationAsDictionary:(NSDictionary *)locationDictionary isGPS:(BOOL)isGPS;

// push data
-(void) pushEventWithType:(NSInteger)eventType andData:(float *)data atHardwareTimestampInNano:(int64_t)timestamp atWallTimeInMicro:(uint64_t)wallTimeInMicros;

// push raw tag streaming data packet
-(void) pushRawTagPacket:(const char*)packet ofLength:(NSInteger)length forMac:(NSString *)macAddress;

// push raw tag status
-(void) pushRawTagStatus:(const char*)packet ofLength:(NSInteger)length;

// push generic json
-(void) pushJSON:(NSData *)json forKey:(NSString *)key;

// push generic json, taking the redirect code as a separate parameter
-(void) pushJSON:(NSData *)json forKey:(NSString *)key withRedirectCode:(CMTFilterEngineRedirectCode)redirectCode;

// push generic json to list key
-(void) pushJSONListEntry:(NSData *)json forKey:(NSString *)key;

// push generic json to list key, taking the redirect code as a separate parameter
-(void) pushJSONListEntry:(NSData *)json forKey:(NSString *)key withRedirectCode:(CMTFilterEngineRedirectCode)redirectCode;

// close data file, returns name of file being closed
-(NSString *) closeFile;

// shutdown the filter engine run loop
-(void) shutdown;

// get current data file name
-(NSString *) getCurrentFileName;

// get FilterEngine timestamp
-(uint64_t) getClockInMicros;

// returns true if clock has been updated from server.
-(bool) clockValid;

// returns server time
-(uint64_t)getServerTimeInMicros;

// returns true if server time is valid
-(bool)serverTimeValid;

// returns tag time
-(uint64_t)getTagTimeInMicros;

// returns true if tag time is valid
-(bool)tagTimeValid;

// returns last 'secondsBack' of ticks data in json format from chosen buffer
-(NSString *)retrieveTicksHistoryFromBuffer:(CMTTickRingBufferType)buffer startingAt:(int)secondsBack;
// returns ticks data in json format since last call to retrieveTicksHistory: or updateTicksHistory: for the same buffer
-(NSString *)updateTicksHistoryFromBuffer:(CMTTickRingBufferType)buffer;

@property (readonly) NSString *filePrefix;
@property (readonly) NSString *etcPrefix;

@end

#pragma once

/*
 *   Event handler upcall and type listing
 */
typedef enum {
  BOOT_COUNT_MISMATCH = 1,
  ACCEL_STUCK_NOTIFY = 2,
  GYRO_STUCK_NOTIFY = 3,
  REQUEST_GPS_SAMPLES = 4,
  IS_DRIVING = 5,            // arg is int: 1= driving, 0= not driving
  TAG_IMPACT_DATA = 6,       // arg is ImpactEvent pointer
  PHONE_ONLY_IMPACT = 7,     // arg is ImpactEvent pointer
} EngineEventType;


typedef enum {
  NOT_DRIVING=0,
  DRIVING=1,
  UNKNOWN=2,
} DriveDetectorState;
